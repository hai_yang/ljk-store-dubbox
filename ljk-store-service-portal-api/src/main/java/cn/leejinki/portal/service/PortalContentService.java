package cn.leejinki.portal.service;


import cn.leejinki.pojo.TbContent;

import java.util.List;

/**
 * 内容分类Service
 * Created by ljk on 18/4/2.
 */
public interface PortalContentService {


    List<TbContent> getContentByCid(long bigAdIndex);
}
