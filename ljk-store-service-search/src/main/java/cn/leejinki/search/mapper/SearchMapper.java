package cn.leejinki.search.mapper;

import cn.leejinki.pojo.SolrItem;

import java.util.List;

/**
 * Solr操作Mapper
 *
 * @author ljk.
 * @create 2018-04-04 下午4:46
 */

public interface SearchMapper {

    List<SolrItem> getSolrItemList();

    SolrItem getSolrItemByItemId(long itemid);

}
