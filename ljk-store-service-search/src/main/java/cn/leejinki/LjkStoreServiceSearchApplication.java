package cn.leejinki;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.solr.SolrAutoConfiguration;
import org.springframework.context.annotation.Configuration;

@Configuration
//@SpringBootApplication(exclude= SolrAutoConfiguration.class)//禁用solr自动配置
@SpringBootApplication
@MapperScan(basePackages = "cn.leejinki.search.mapper")
public class LjkStoreServiceSearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(LjkStoreServiceSearchApplication.class, args);
	}
}
