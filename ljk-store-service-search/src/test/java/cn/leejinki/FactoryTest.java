package cn.leejinki;

//import cn.leejinki.search.service.impl.SearchServiceImpl;
//import com.alibaba.dubbo.config.annotation.Service;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
//import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
//import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FactoryTest {

    private static Logger logger = Logger.getLogger(FactoryTest.class);

    // zookeeper地址
    private static String zkHostString = "192.168.0.103:2181";//,192.168.0.101:3001,192.168.0.101:3003";
    // collection默认名称
    private static String defaultCollection = "collect1";


    public static void  main(String[] args) throws Exception{

        try {

            String solrUrl="http://192.168.0.103:8983/solr/collect1";
            HttpSolrClient httpSolrClient=new HttpSolrClient.Builder(solrUrl)
                    .withConnectionTimeout(5000)
                    .withSocketTimeout(5000).build();

            //solrClient=httpSolrClient;

            SolrInputDocument document = new SolrInputDocument();

            document.addField("id", "114");
            document.addField("item_category_name", "phone2");
            document.addField("item_title", "abcd4");

            String image = "addiskdkls.jpg";
            String[] split = image.split(",");

            document.addField("item_image", split[0]);
            document.addField("item_price", 50);
            document.addField("item_sell_point", "abc22");
            document.addField("item_desc", "ddis");

            //solrClient.add(document);
            httpSolrClient.add(document);

            httpSolrClient.commit();

            SolrQuery query=new SolrQuery();
            query.set("q","*:*");
            query.set("start","0");
            query.set("rows","10");
            QueryResponse response=httpSolrClient.query(query);
            SolrDocumentList docList=response.getResults();
            System.out.println("docment num:"+docList.getNumFound());


        }catch (Exception e) {
            e.printStackTrace();
        }
    }

}
