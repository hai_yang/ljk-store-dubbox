package cn.leejinki.solr;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication
@MapperScan(basePackages = "cn.leejinki.solr.search.mapper")
public class LjkStoreServiceSolrSearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(LjkStoreServiceSolrSearchApplication.class, args);

	}
}
