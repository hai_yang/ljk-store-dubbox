package cn.leejinki;

//import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication
public class LjkStoreWebItemApplication {

	public static void main(String[] args) {
		SpringApplication.run(LjkStoreWebItemApplication.class, args);
	}
}
