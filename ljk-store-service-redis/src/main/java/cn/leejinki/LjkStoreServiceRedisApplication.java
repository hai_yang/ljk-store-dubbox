package cn.leejinki;

import com.alibaba.dubbo.container.Main;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication
public class LjkStoreServiceRedisApplication {

    public static void main(String[] args) {
        SpringApplication.run(LjkStoreServiceRedisApplication.class, args);

        Main.main(args);
    }
}
