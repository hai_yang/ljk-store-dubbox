package cn.leejinki.redis.test;

import redis.clients.jedis.Jedis;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class RedisConnTest {
    public static void main(String[] args){
        //connect local redis server
        Jedis jedis=new Jedis("localhost",6379);
        jedis.auth("123456");
        System.out.println("conn sucess");
        //view server run status
        System.out.println("server is running:"+jedis.ping());
        //print key num
        System.out.println("key num:"+jedis.dbSize());
        //list all key
        Set<String> keys=jedis.keys("*");
        Iterator<String> iter=keys.iterator();
        while (iter.hasNext()){
            String key=iter.next();
            System.out.println(key);
        }
        //存储数据到列表中
        jedis.lpush("site-list", "Runoob");
        jedis.lpush("site-list", "Google");
        jedis.lpush("site-list", "Taobao");
        // 获取存储的数据并输出
        List<String> list = jedis.lrange("site-list", 0 ,2);
        for(int i=0; i<list.size(); i++) {
            System.out.println("列表项为: "+list.get(i));
        }

    }
}
