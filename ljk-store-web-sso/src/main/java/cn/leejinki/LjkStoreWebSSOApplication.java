package cn.leejinki;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication
public class LjkStoreWebSSOApplication {

	public static void main(String[] args) {
		SpringApplication.run(LjkStoreWebSSOApplication.class, args);
	}
}
