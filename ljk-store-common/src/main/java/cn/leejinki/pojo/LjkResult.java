package cn.leejinki.pojo;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Serializable;

/**
 * 自定义响应结构
 */
public class LjkResult implements Serializable {

    // 定义jackson对象
    private static final ObjectMapper MAPPER = new ObjectMapper();

    // 响应业务状态
    private Integer status;

    // 响应消息
    private String msg;

    // 响应中的数据
    private Object data;

    public static LjkResult build(Integer status, String msg, Object data) {
        return new LjkResult(status, msg, data);
    }

    public static LjkResult ok(Object data) {
        return new LjkResult(data);
    }

    public static LjkResult ok() {
        return new LjkResult(null);
    }

    public LjkResult() {

    }

    public static LjkResult build(Integer status, String msg) {
        return new LjkResult(status, msg, null);
    }

    public LjkResult(Integer status, String msg, Object data) {
        this.status = status;
        this.msg = msg;
        this.data = data;
    }

    public LjkResult(Object data) {
        this.status = 200;
        this.msg = "OK";
        this.data = data;
    }

    public Boolean isOK() {
        return this.status == 200;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
