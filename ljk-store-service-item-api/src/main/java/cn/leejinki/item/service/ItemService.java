package cn.leejinki.item.service;

import cn.leejinki.pojo.TbItem;
import cn.leejinki.pojo.TbItemDesc;

/**
 * 商品 Service
 *
 * @author ljk.
 * @update 2018-04-06
 */

//@Path("/ItemService")
//@Produces({ContentType.APPLICATION_JSON_UTF_8,ContentType.TEXT_XML_UTF_8})
public interface ItemService {

    //http://localhost:8514/item/ItemService/getItemById/{id}
    //@POST()
    //@Path("/getItemById/{id}")
    TbItem getItemById(/**@PathParam("id")*/Long itemId);

    //http://localhost:8514/item/ItemService/getItemDescById/{id}
    //@POST()
    //@Path("/getItemDescById/{id}")
    TbItemDesc getItemDescById(/**@PathParam("id")*/Long itemId);


}
