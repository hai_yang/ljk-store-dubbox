package cn.leejinki.redis.service;

/**
 * Jedis 接口
 * Updated by ljk on 2018/3/28.
 */
public interface JedisClient {

    String get(String key);

    String set(String key, String value);

    String hget(String hkey, String key);//获取存储在哈希表中指定字段的值:hget key field

    long hset(String hkey, String key, String value);//将哈希表 key 中的字段 field 的值设为 value:hset key field value

    long incr(String key);//递增指令：incr（默认从0开始

    long expire(String key, Integer second);//为给定 key 设置过期时间。expire key seconds

    long ttl(String key);//以秒为单位，返回给定 key 的剩余生存时间(TTL, time to live)。:ttl key

    long del(String key);//该命令用于在 key 存在时删除 key:del key

    long hdel(String hkey, String key);//删除一个或多个哈希表字段:hdel key field1[field2]

}
