package cn.leejinki;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@SpringBootApplication
@EnableTransactionManagement
@MapperScan(basePackages = "cn.leejinki.mapper")
public class LjkStoreServiceSSOApplication {

	public static void main(String[] args) {
		SpringApplication.run(LjkStoreServiceSSOApplication.class, args);

	}
}
